<?php

namespace App\Http\Controllers\Dashboard\Pengaturan;
use App\Http\Controllers\Controller;

use App\Sales;

use App\Mywifi;
use App\Paket;
use DateTime;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'paket' => Paket::paginate(10),
        ];
        return view('pengaturan.paket', $data);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           
            'nama_paket' => 'required|string',
            'deskripsi' => 'string',
            'harga_paket' => 'string',
            
        ]);

        $paket = new Paket ([
        
            'nama_paket' => $request->nama_paket,
            'deskripsi' => $request->deskripsi,
            'harga_paket' => $request->harga_paket,

        ]);


        $paket->save();

 
		return redirect()->back()->with('success', 'Data PAKET berhasil di tambah !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function show(Paket $paket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paket = Paket::findOrFail($id);
        return view ('pengaturan.editpaket', compact('paket','id') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paket = Paket::find($id);
        $paket->nama_paket =$request->get('nama_paket');
        $paket->deskripsi =$request->get('deskripsi');
        $paket->harga_paket =$request->get('harga_paket');

        $paket->save();
        
        return redirect('/pengaturan/paket')->with('success', 'Data berhasil di Ubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paket  $paket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $paket = Paket::findOrFail($id);
        $paket->delete();
        return redirect('/pengaturan/paket')->with('success', 'Data berhasil di Hapus');
    }
}
