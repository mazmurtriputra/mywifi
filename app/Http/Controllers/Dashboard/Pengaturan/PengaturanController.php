<?php

namespace App\Http\Controllers\Dashboard\Pengaturan;
use App\Http\Controllers\Controller;

use App\Sales;

use App\Mywifi;
use DateTime;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PengaturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'sales' => Sales::paginate(10),
        ];
        return view('pengaturan.index', $data);
        
    }

    public function sales()
    {
        $data = [
            'sales' => Sales::paginate(10),
        ];
        return view('pengaturan.sales', $data);
        
    }
    
    public function salesstore(Request $request)
    {
        $this->validate($request,[
           
            'kode_sales' => 'required|string',
            'nama_sales' => 'string',
            'status' => 'string',
            'target' => 'string',
            'no_hp' => 'string',
            'wilayah' => 'string',

            
        ]);

        $sales = new Sales ([
        
            'kode_sales' => $request->kode_sales,
            'nama_sales' => $request->nama_sales,
            'status' => $request->status,
            'target' => $request->target,
            'no_hp' => $request->no_hp,
            'wilayah' => 'string',


        ]);


        $sales->save();

 
		return redirect()->back()->with('success', 'Data SALES berhasil di tambah');
    }

    public function salesupdate(Request $request, $id)
    {
        $sales = Sales::find($id);
        $sales->kode_sales =$request->get('kode_sales');
        $sales->nama_sales =$request->get('nama_sales');
        $sales->target =$request->get('target');
        $sales->no_hp =$request->get('no_hp');
        $sales->wilayah =$request->get('wilayah');

        $sales->save();
        
        return redirect()->back()->with('success', 'Data berhasil di Ubah');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sales = Sales::findOrFail($id);

        return view('pengaturan.detail', compact ('sales'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function salesdelete($id)
    {
        $sales = Sales::findOrFail($id);
        $sales->delete();
        return redirect('/pengaturan/sales')->with('success', 'Data berhasil di Hapus');
    }
}
