<?php

namespace App\Http\Controllers\Dashboard\Exist;


use App\Exist;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  [
            'exist' => Exist::paginate(12),
        ];
        return view('existing.index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Exist::findOrFail($id);

        return view('existing.profile', compact ('data'));
    }

    public function existsearch(\Illuminate\Http\Request $request)

    {
        $this->validate($request, [
            'limit' => 'integer',
        ]);
        
        $exist = Exist::when($request->keyword, function ($query) use ($request) {
            $query->where('NAMAPELANGGAN', 'like', "%{$request->keyword}%") // search by email
                ->orWhere('CAT', 'like', "%{$request->keyword}%")
                ->orWhere('INETSID', 'like', "%{$request->keyword}%"); // or by name  // or by name
        })->paginate($request->limit ? $request->limit : 10);

        $exist->appends($request->only('keyword'));

        return view('existing.index', compact('exist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
