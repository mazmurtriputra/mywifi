<?php

namespace App\Http\Controllers\Dashboard\Registrasi;
use App\Http\Controllers\Controller;


use App\Wifi;

use App\Mywifi;
use App\Sales;
use App\Paket;
use DateTime;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class WifiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('registrasi.index');
    }

   
    public function create()
    {
        $sales = Sales::all();
        $pakets = Paket::all();
        
        return view('registrasi.wmscreate', compact('sales','pakets'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function prospek()
    
    {

        $prospek = Mywifi::where('cust_status', 'PROSPEK')->paginate(10);
        
        return view('registrasi.prospek', compact ('prospek'));
    
    }

    public function wms()
    
    {

        $data = [
            'wms' => Mywifi::paginate(10),
            'sales' => Sales::paginate(10),
            'pakets' => Paket::paginate(10),
        ];
        return view('registrasi.wms', $data);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function prospekstore(Request $request)
    {
        $this->validate($request,[
           
            'nama_plg' => 'required|string',
            'alamat' => 'string',
            'no_hp' => 'string',
            'email' => 'string',
            'foto_pelanggan' => 'string',
            'cust_status' => 'string',
            
        ]);

        $prospek = new Mywifi ([
        
            'nama_plg' => $request->nama_plg,
            'alamat' => $request->alamat,
            'no_hp' => $request->no_hp,
            'email' => $request->email,
            'cust_status' => $request->cust_status,
            'foto_pelanggan' => '["noimage.png"]',

        ]);


        $prospek->save();

 
		return redirect()->back()->with('success', 'Data PROSPEK berhasil di tambah');
    }

    public function store2(Request $request)
    {

        $messages = [
            'required' => ':attribute wajib diisi !!!',
            'unique' => 'Email Duplikat',
        ];
        $this->validate($request, [

            'nama_plg' => 'required|string',
            'alamat' => 'string',
            'no_hp' => 'string',
            'email' => 'required|string',
            'tgl_regis' => 'date',
            'jml_ap' => 'string',
            'biaya_perbulan' => 'string',
            'ssid' => 'string',
            'password' => 'string',
            'nama_sales' => 'string',
            'cust_status' => 'string',
            'inet' => 'string',
            'odp' => 'string',
            'status' => 'string',
            'ket' => 'string',
            'id_sales' => 'string',
            'id_pakets' => 'string',
            'foto_pelanggan' => 'required',
            'foto_pelanggan.*' => 'file|mimes:docx,xlsx|max:6048'

        ],$messages);
        
        if($request->hasfile('foto_pelanggan'))
        {
            foreach($request->file('foto_pelanggan') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/files/', $name);  // your folder path
                $data[] = $name;  
            }
        }
        
        $wms = new Mywifi;
        $wms->nama_plg = $request->nama_plg;
        $wms->alamat = $request->alamat;
        $wms->no_hp = $request->no_hp;
        $wms->email = $request->email;
        $wms->tgl_regis = $request->tgl_regis;
        $wms->jml_ap = $request->jml_ap;
        $wms->total = $request->total;
        $wms->ssid = $request->ssid;
        $wms->odp = $request->odp;
        $wms->status = $request->status;
        $wms->ket = $request->ket;
        $wms->inet = $request->inet;
        $wms->id_sales = $request->id_sales;
        $wms->id_pakets = $request->id_pakets;
        $wms->password = $request->password;
        $wms->nama_sales = $request->nama_sales;

        $wms->cust_status = $request->cust_status;
        $wms->foto_pelanggan = json_encode($data);

        $wms->save();

        return redirect('/registrasi/wms')->with('success', 'Pelanggan baru berhasil di Simpan !');
    }

    public function wmssearch(\Illuminate\Http\Request $request)

    {
        $this->validate($request, [
            'limit' => 'integer',
        ]);

        
        $wms = Mywifi::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_plg', 'like', "%{$request->keyword}%") // search by email
                ->orWhere('no_hp', 'like', "%{$request->keyword}%")
                ->orWhere('cust_status', 'like', "%{$request->keyword}%")
                ->orWhere('layanan', 'like', "%{$request->keyword}%"); // or by name  // or by name
        })->paginate($request->limit ? $request->limit : 10);

        $sales = Sales::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_sales', 'like', "%{$request->keyword}%") // search by email
                ->orWhere('kode_sales', 'like', "%{$request->keyword}%");
        })->paginate($request->limit ? $request->limit : 10);

        $wms->appends($request->only('keyword'));
        $sales->appends($request->only('keyword'));


        return view('registrasi.wms', compact('wms','sales'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Mywifi::findOrFail($id);

        return view('registrasi.profile', compact ('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prospek = Mywifi::findOrFail($id);
        return view ('registrasi.prospekedit', compact('prospek','id') );
    }

    public function editwms($id)
    {
        $wms = Mywifi::findOrFail($id);
        return view ('registrasi.wmsedit', compact('wms','id') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function prospekupdate(Request $request, $id)
    {
        $prospek = Mywifi::find($id);
        $prospek->nama_plg =$request->get('nama_plg');
        $prospek->alamat =$request->get('alamat');
        $prospek->no_hp =$request->get('no_hp');
        $prospek->email =$request->get('email');
        $prospek->cust_status =$request->get('cust_status');
        $prospek->save();
        
        return redirect()->back()->with('success', 'Data berhasil di Ubah');
    }

    public function updatewms(Request $request, $id)
    {
        $wms = Mywifi::find($id);
        $wms->nama_plg =$request->get('nama_plg');
        $wms->alamat =$request->get('alamat');
        $wms->no_hp =$request->get('no_hp');
        $wms->email =$request->get('email');
        $wms->cust_status =$request->get('cust_status');
        $wms->kecepatan =$request->get('kecepatan');
        $wms->layanan =$request->get('layanan');
        $wms->jml_ap =$request->get('jml_ap');
        $wms->biaya_perbulan =$request->get('biaya_perbulan');
        $wms->ppn =$request->get('ppn');
        $wms->total =$request->get('total');
        $wms->ssid =$request->get('ssid');
        $wms->status =$request->get('status');
        $wms->ket =$request->get('ket');
        $wms->inet =$request->get('inet');
        $wms->odp =$request->get('odp');
        $wms->password =$request->get('password');
        $wms->no_hp =$request->get('no_hp');
        $wms->save();
        
        return redirect()->back()->with('success', 'Data Pelanggan berhasil di Simpan !');
    }

    public function updatewmsphoto(Request $request, $id)
    {
        $wms = Mywifi::find($id);
        
        $wms->foto_pelanggan =$request->get('foto_pelanggan');

        if ($request->hasfile('foto_pelanggan')!=null)
        {
            foreach($request->file('foto_pelanggan') as $image)
            {
                $name=$image->getClientOriginalName();
                $image->move(public_path().'/images/', $name);  // your folder path
                $data[] = $name;  
            }
        }
        else
        {
            return redirect()->back()->with('danger', 'Foto BELUM DI UPDATE !');
        }

        $wms->foto_pelanggan = json_encode($data);
        $wms->save();
        
        return redirect()->back()->with('success', 'Data Pelanggan berhasil di Simpan !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroywms($id)
    {
        $wms = Mywifi::findOrFail($id);
        $wms->delete();
        return redirect('/registrasi/wms')->with('success', 'Data berhasil di Hapus');
    }
}
