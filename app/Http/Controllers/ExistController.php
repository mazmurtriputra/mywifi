<?php

namespace App\Http\Controllers;
use App\Exist;

use Illuminate\Http\Request;

class ExistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data =  [
            'exist' => Exist::paginate(10),
        ];
        return view('existing.index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function existsearch(\Illuminate\Http\Request $request)

    {
        $this->validate($request, [
            'limit' => 'integer',
        ]);
        
        $data = Exist::when($request->keyword, function ($query) use ($request) {
            $query->where('NAMAPELANGGAN', 'like', "%{$request->keyword}%") // search by email
                ->orWhere('CAT', 'like', "%{$request->keyword}%")
                ->orWhere('INETSID', 'like', "%{$request->keyword}%"); // or by name  // or by name
        })->paginate($request->limit ? $request->limit : 10);

        $data->appends($request->only('keyword'));

        return view('existing.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [
            'exist' => $exist,
            'digit_id' => $digit_id,
            'physicians' => $physicians, 
            'families' => $families,
            'family_types' => $family_types,
            'medical_record' => $medical_record,
            // medical record attr'
            'payment_types' => $payment_types,
            'exist_classes' => $exist_classes,
            'record_in_out_types' => $record_in_out_types,
            'outexist_polies' => $outexist_polies,
            'inexist_rooms' => $inexist_rooms,
            'inexist_beds' => $inexist_beds,
        ];

        return view('dashboard.exist.profile', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
