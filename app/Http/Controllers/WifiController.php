<?php

namespace App\Http\Controllers;
use App\Mywifi;

use DateTime;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class WifiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        

        return view('registrasi.index');
    }
    public function wmssearch(\Illuminate\Http\Request $request)

    {
        $this->validate($request, [
            'limit' => 'integer',
        ]);
        
        $data = Mywifi::when($request->keyword, function ($query) use ($request) {
            $query->where('nama_plg', 'like', "%{$request->keyword}%") // search by email
                ->orWhere('no_hp', 'like', "%{$request->keyword}%")
                ->orWhere('layanan', 'like', "%{$request->keyword}%"); // or by name  // or by name
        })->paginate($request->limit ? $request->limit : 10);

        $data->appends($request->only('keyword'));

        return view('registrasi.wms', compact('data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registrasi.prospekcreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
