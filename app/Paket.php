<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = "pakets";

    protected $primaryKey = 'id';

    protected $fillable = [
        'nama_paket',    
        'deskripsi',
        'harga_paket',
        'diskon',
    ];

    public function wms()
    {
        return $this->hasMany(Mywifi::class, 'id_pakets'); //arahkan ke table child detail_category
    }
}
