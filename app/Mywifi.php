<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Mywifi extends Model
{

    protected $table = "mywifi";

    protected $primaryKey = 'id';

    protected $fillable = [
        'nama_plg',
        'alamat',
        'no_hp',
        'cust_status',
        'email',
        'kecepatan',
        'layanan',
        'jml_ap',
        'biaya_perbulan',
        'ppn',
        'total',
        'ssid',
        'password',
        'foto_pelanggan',
        'longtitude',
        'latitude',
        'kode_sales',    
        'nama_sales',
        'tgl_regis',
        'inet',
        'odp',
        'status',
        'ket',
    ];

    public function sales(){
        return $this->belongsTo('App\Sales', 'id_sales');
    }

    public function paket(){
        return $this->belongsTo('App\Paket', 'id_pakets');
    }

}
