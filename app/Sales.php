<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = "sales";

    protected $primaryKey = 'id';

    protected $fillable = [
        'kode_sales',    
        'nama_sales',
        'status',
        'target',
        'no_hp',
        'wilayah',
    ];

    public function wms()
    {
        return $this->hasMany(Mywifi::class, 'id_sales'); //arahkan ke table child detail_category
    }

}
