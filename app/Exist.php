<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exist extends Model
{
 
    protected $table = "db_wifiid_bpn";

    public $timestamps = true;

    protected $primaryKey = 'id';

}
