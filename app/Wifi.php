<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wifi extends Model
{
    protected $table = "mywifi";

    public $timestamps = true;

    protected $primaryKey = 'id';

    protected $fillable = ['foto_pelanggan'];


}
