<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Exist;

$factory->define(App\Exist::class, function (Faker\Generator $faker) {
    return [
        'id' => $faker->randomNumber,
    ];
});
