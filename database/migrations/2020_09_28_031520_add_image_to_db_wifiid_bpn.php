<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddImageToDbWifiidBpn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('db_wifiid_bpn', function (Blueprint $table) {
            $table->string('FOTO_RUMAH');
            $table->string('FOTO_PELANGGAN');
            $table->string('FOTO_ODP');
            $table->string('TTD_SALES');
            $table->string('TTD_PELANGGAN');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('db_wifiid_bpn', function (Blueprint $table) {
            //
        });
    }
}
