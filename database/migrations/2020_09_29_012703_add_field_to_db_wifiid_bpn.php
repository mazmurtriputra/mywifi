<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToDbWifiidBpn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('db_wifiid_bpn', function (Blueprint $table) {
            $table->string('EMAIL');
            $table->string('CUST_STATUS');
            $table->string('FOTO');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('db_wifiid_bpn', function (Blueprint $table) {
            //
        });
    }
}
