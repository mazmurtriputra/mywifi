<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMywifiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wms', function (Blueprint $table) {
            $table->id();
            $table->string('nama_plg');
            $table->string('no_hp');
            $table->string('email')->unique();
            $table->string('alamat');
            $table->string('layanan');
            $table->string('kecepatan');
            $table->string('jml_ap');
            $table->string('biaya_perbulan');
            $table->string('ppn');
            $table->string('total');
            $table->string('ssid');
            $table->string('password');
            $table->string('tgl_regis');
            $table->string('foto_pelanggan');
            $table->string('latitude', 15)->nullable();
            $table->string('longitude', 15)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
