<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToMywifi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mywifi', function (Blueprint $table) {
            $table->string('odp');
            $table->string('status');
            $table->string('inet');
            $table->string('ket');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mywifi', function (Blueprint $table) {
            //
        });
    }
}
