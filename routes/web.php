<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('wmssearch', 'WifiController@wmssearch')->name('wmssearch');

Route::middleware('auth')->name('dashboard.')->namespace('Dashboard')->group(function () {
    
    // Main Menu 👌
    Route::get('/', 'HomeController@index')->name('index');
    Route::get('home', 'HomeController@index');
    // Route::get('exist', [App\Http\Controllers\ExistController::class, 'index'])->name('exist');
    
    // Patient 🚧
    Route::prefix('exist')->name('exist')->namespace('Exist')->group(function () {
        // page view 👌
        Route::get('/', 'ExistController@index');
        Route::get('profil/{id}', 'ExistController@show')->name('.profile');
        Route::get('existsearch', 'ExistController@existsearch')->name('.existsearch');
    });

    Route::prefix('roadmap')->name('roadmap')->namespace('Roadmap')->group(function () {
        // page view 👌
        Route::get('/', 'RoadmapController@index');
        Route::get('profil/{id}', 'ExistController@show')->name('.profile');
    });

    Route::prefix('laporan')->name('laporan')->namespace('Laporan')->group(function () {
        // page view 👌
        Route::get('/', 'LaporanController@index');
    });

    Route::prefix('manajemen')->name('manajemen')->namespace('Manajemen')->group(function () {
        // page view 👌
        Route::get('/', 'ManajemenController@index');
    });
    
    Route::prefix('pengaturan')->name('pengaturan')->namespace('Pengaturan')->group(function () {
        // page view 👌
        Route::get('/', 'PengaturanController@index');
        Route::get('sales', 'PengaturanController@sales')->name('.sales');
        Route::post('salesstore', 'PengaturanController@salesstore')->name('.salesstore');
        Route::post('salesupdate/{id}', 'PengaturanController@salesupdate')->name('.salesupdate');
        Route::get('salesdelete/{id}', 'PengaturanController@salesdelete')->name('.salesdelete');
        Route::get('salesdetail/{id}', 'PengaturanController@show')->name('.salesdetail');

        Route::get('paket', 'PaketController@index')->name('.paket');
        Route::post('paketstore', 'PaketController@store')->name('.paketstore');
        Route::post('paketupdate/{id}', 'PaketController@update')->name('.paketupdate');
        Route::get('paketdelete/{id}', 'PaketController@destroy')->name('.paketdelete');
        Route::get('paketedit/{id}', 'PaketController@edit')->name('.paketedit');
    });

    Route::prefix('registrasi')->name('registrasi')->namespace('Registrasi')->group(function () {
        // page view 👌
        Route::get('/', 'WifiController@index');
        Route::get('prospek', 'WifiController@prospek')->name('.prospek');

        Route::get('prospekedit/{id}', 'WifiController@edit')->name('.prospekedit');
        Route::post('prospekupdate/{id}', 'WifiController@prospekupdate')->name('.prospekupdate');
        Route::get('profil/{id}', 'WifiController@show')->name('.profile');
        Route::get('wmscreate', 'WifiController@create')->name('.wmscreate');

        Route::get('wmsedit/{id}', 'WifiController@editwms')->name('.wmsedit');
        Route::get('wms', 'WifiController@wms')->name('.wms');
        Route::post('wmsupdate/{id}', 'WifiController@updatewms')->name('.wmsupdate');
        Route::post('wmsupdatephoto/{id}', 'WifiController@updatewmsphoto')->name('.wmsupdatephoto');
        Route::get('wmsdelete/{id}', 'WifiController@destroywms')->name('.wmsdelete');
        Route::get('wmssearch', 'WifiController@wmssearch')->name('.wmssearch');

        
        Route::post('store', 'WifiController@store')->name('.store');
        Route::post('prospekstore', 'WifiController@prospekstore')->name('.prospekstore');
        Route::post('wmsstore', 'WifiController@store2')->name('.wmsstore');

    });


});