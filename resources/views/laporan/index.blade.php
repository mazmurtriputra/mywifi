
@extends('layouts.app')

@section('content')


{{--List User --}}

<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Laporan</h3>
            <hr class="my-0">
        </div>
    </div>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
          <li class="breadcrumb-item active" aria-current="page">Laporan</li>
        </ol>
      </nav>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-5 pb-0">
                <div class="dashboard card-body">

                    {{-- Pasien --}}
                    <div class="report-group mb-5">
                        <h5 class="text-purple-old">
                            Prospek
                        </h5>
                        <hr class="mt-1">
                        {{-- list report --}}
                        <ul class="list-group">
                            <a href="#" class="list-group-item list-group-item-action">Daftar Seluruh Prospek</a>
                            <a href="#" class="list-group-item list-group-item-action">Daftar Prospek Bulan ini</a>
                            <a href="#" class="list-group-item list-group-item-action">Daftar Prospek Tahun ini</a>
                          </ul>
                    </div>
                    
                    {{-- Doctor --}}
                    <div class="report-group mb-5">
                        <h5 class="text-purple-old">
                            Wms
                        </h5>
                        <hr class="mt-1">
                        {{-- list report --}}
                        <ul class="list-group">
                            <a href="#" class="list-group-item list-group-item-action">Daftar seluruh WMS</a>
                            <a href="#" class="list-group-item list-group-item-action">Daftar WMS bulan ini</a>
                            <a href="#" class="list-group-item list-group-item-action">Daftar WMS Tahun ini</a>
                        </ul>
                    </div>

                    <div class="report-group mb-5">
                        <h5 class="text-purple-old">
                            Rapor Sales
                        </h5>
                        <hr class="mt-1">
                        {{-- list report --}}
                        <ul class="list-group">
                            <a href="#" class="list-group-item list-group-item-action">Sales Terbaik</a>
                            <a href="#" class="list-group-item list-group-item-action">Sales On Target</a>
                            <a href="#" class="list-group-item list-group-item-action">Sales not on Target</a>
                        </ul>
                    </div>
            
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


