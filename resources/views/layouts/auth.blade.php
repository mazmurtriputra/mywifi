<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link rel="icon" href="{{ asset('images/logo.png') }}" type="image/gif">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

        <title>{{ config('app.name', 'My WIFI') }}</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>

    <body>

        <div id="auth">

            {{-- Dashboard's Page --}}
            @yield('page')
            
        </div>

        {{-- Footer --}}
        @include('partials._footer')

        <!-- Scripts -->
    </body>

</html>