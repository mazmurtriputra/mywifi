<div class="footer m-0 row justify-content-center">
    <div class="col-md-12 col-lg-12 px-5">
        <footer class="py-0 px-3 border-0">
            <div class="d-flex">
                <ul class="nav ml-auto">
                    <li class="nav-item copyright">
                        <div class="nav-link px-0">
                            <span class="copyright text-black">
                                WITEL BALIKPAPAN © {{ date('Y') }} 
                            </span>
                        </div>
                    </li>
                </ul>
            </div>
        </footer>
    </div>
</div>