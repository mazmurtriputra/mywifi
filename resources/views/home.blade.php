@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                <a href="{{ route('dashboard.registrasi')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Registrasi Baru
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa fa-file-text-o px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Registrasi</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.exist')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Data Pelanggan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-users px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Data Existing</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.laporan')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Laporan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa fa-paperclip px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Laporan Transaksi</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.roadmap')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Peta Road Map
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-map-marker px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Peta Roadmap Pelanggan</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.pengaturan')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Pengaturan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-cog px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Pengaturan Dashboard</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.manajemen')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                               Manajemen
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-briefcase px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Manajemen</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
        </div>
    </div>
</div>
@endsection
