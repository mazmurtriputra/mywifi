<ul class="option-bar navbar-nav ml-auto mt-lg-0 text-right">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="patient-option" role="button"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="patient-option">
            {{-- <a class="dropdown-item {{ set_active([
                'dashboard.patient.profile',
                ]) }}" href="{{ route('dashboard.patient.profile', ['id' => $patient->id]) }}">
                <i class="fa fa-user text-dark"></i> Profil
            </a>
            <a class="dropdown-item {{ set_active([
                ]) }}" href="{{ route('dashboard.patient.profile', ['id' => $patient->id]) }}">
                <i class="fa fa-calculator text-dark"></i> Tagihan 
                <span class="badge badge-sm badge-warning">
                    {{ $patient->unpaid }}
                </span>
            </a> --}}
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" target="_blank">
                <i class="fa fa-id-card text-secondary"></i> Kartu Pasien
            </a>
            <a class="dropdown-item" href="#" target="_blank">
                <i class="fa fa-list-alt text-secondary"></i> Kartu Berobat
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-patient">
                <i class="fa fa-pencil-square text-success"></i> Sunting
            </a>
            <a class="dropdown-item btn-delete" data-id="delete-patient" style="cursor: pointer;">
                <i class="fa fa-trash text-danger"></i>
                Hapus
            </a>
            <form id="delete-patient"
                action="#" method="POST"
                style="display: none;">
                @csrf
                {{ method_field('DELETE') }}
                <input type="hidden" name='id' value="adsd">
            </form>
        </div>
    </li>
</ul>