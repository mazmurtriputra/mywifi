@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Data Existing Pelanggan</h3>
            <hr class="my-0">
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
          <li class="breadcrumb-item"><a href="/exist">Existing</a></li>
        </ol>
      </nav>

      <div class="row">
        <form action="{{ route('dashboard.exist.existsearch') }}" method="GET" class="col-md-8 col-lg-9">
            <div class="row">
                <div class="col-md-6 col-lg-7 mb-4">
                    <div class="card shadow filter-field border-0">
                        <div class="form-row align-items-center">
                            <div class="field col-md-4 p-0">
                                <select name="jenis-kelamin" class="custom-select border-0">
                                    <option value="semua" selected>Nama</option>
                                </select>
                            </div>
                            <div class="field col-md-4 p-0">
                                <select name="otoritas" class="custom-select border-0">
                                    <option value="semua" selected>Jenis</option>
                                </select>
                            </div>
                            <div class="field col-md-4 p-0">
                                <select name="status" class="custom-select border-0">
                                    <option value="semua" selected>Status</option>
                                    <option value="aktif" {{ request('status') == 'aktif' ? 'selected' : '' }}>Prospek</option>
                                    <option value="tidak-aktif" {{ request('status') == 'tidak-aktif' ? 'selected' : '' }}>Langganan</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5 mb-4">
                    <div class="card shadow search-field border-0">
                        <div class="form-row align-items-center">
                            <div class="col-10 p-0">
                                <input type="text" name="keyword" value="{{ request('keyword') }}" class="form-control border-0" id="inlineFormInput" placeholder="Pencarian">
                            </div>
                            <div class="col-2 p-0">
                                <button type="submit" class="btn w-100 bg-purple-old text-light"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    
    @if (count($exist) > 0)
<div class="row mb-5">
    @foreach ($exist as $data)
    <div class="col-lg-4 col-md-6 mb-4">
        <div class="data card shadow" style="border-radius: 2rem;">
            <a href="{{ route('dashboard.exist.profile', ['id' => $data->id]) }}" class="card-header bg-purple-old py-2" style="border-radius: 2rem 2rem 0rem 0rem;">
                <h5 class="text-white mb-0">
                   {{ $data->NAMAPELANGGAN }} 
                </h5>
               

                @if ($data->unpaid > 0)
                <span class="badge m-0 badge-sm badge-warning">
                    {{-- {{ $data->unpaid }} tagihan --}}
                </span>
                @endif
            </a>
            <div class="card-body" style="height:225px;">
                {{-- Name --}}
                
                {{-- ID --}}
                <div class="media">
                    <h6 class="mr-2">
                        <i class="fa fa-map-marker"></i>
                    </h6>
                    <div class="media-body">
                        <h6 class="my-0 text-secondary">KOORDINAT</h6>
                        <p class="mb-0">
                            {{ $data->KOORDINAT ?? 'Koordinat Kosong' }}
                        </p>
                        <hr class="mt-0 mb-2">
                    </div>
                </div>
                {{-- Birthday --}}
                <div class="media">
                    <h6 class="mr-2">
                        <i class="fa fa-phone"></i>
                    </h6>
                    <div class="media-body">
                        <h6 class="my-0 text-secondary">Nomor HP</h6>
                        <p class="mb-0">
                            {{ $data->NO_HP ?? 'No HP Kosong' }}
                           
                        </p>
                        <hr class="mt-0 mb-2">
                    </div>
                </div>
                {{-- Address --}}
                <div class="media">
                    <h6 class="mr-2">
                        <i class="fa fa-map"></i>
                    </h6>
                    <div class="media-body">
                        <h6 class="my-0 text-secondary">Alamat Pelanggan</h6>
                        <p class="mb-0">
                            {{ $data->ALAMAT }} -
                           
                        </p>
                    </div>
                </div>
                <div class="media">
                    <h6 class="mr-2">
                        <i class="fa fa-map"></i>
                    </h6>
                    <div class="media-body">
                        <h6 class="my-0 text-secondary">Status</h6>
                        <p class="mb-0 badge m-0 badge-sm badge-success">
                            {{ $data->STATUS }} 
                           
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    {{-- Pagination link --}}
    <div class="col-12 mt-4">
        {!! $exist->appends(request()->input())->links() !!}
    </div>
</div>
@endif
</div>
@endsection
