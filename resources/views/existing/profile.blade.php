@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Data Detail Pelanggan</h3>
            <hr class="my-0">
        </div>
    </div>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
          <li class="breadcrumb-item"><a href="/exist">Existing</a></li>
        </ol>
      </nav>
                      <a class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Profil Pelanggan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="bio row mb-4">
                         
                                    <div class="col-md-6">
                                        <div class="media">
                                            <img src="{{ asset('images/user.png') }}" alt="..." class="img-thumbnail rounded-circle mr-1" style="height: 100px">
                                        
                                            <div class="media-body" style="margin-top: 2rem;">
                                                <h6 class="my-0 text-secondary font-weight-bold">Nama Lengkap</h6>
                                                <h4 class="mb-0 text-secondary">
                                                    {{ $data->NAMAPELANGGAN }}
                                                </h4>
                                                <hr class="mt-0 mb-2">  
                                            </div>
                                        </div>

                                        <div class="media" style="margin-top: 1rem;">
                                            <h6 class="mr-2">
                                                <i class="fa fa-chrome text-secondary"></i>
                                            </h6>
                                            <div class="media-body">
                                                <h6 class="my-0 text-secondary font-weight-bold">Nomor Internet</h6>
                                                <p class="mb-0 text-secondary">
                                                    {{ $data->INETSID ?? '-' }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                        
                                        <div class="media" style="margin-top: 1rem;">
                                            <h6 class="mr-2">
                                                <i class="fa fa-map-marker text-secondary"></i>
                                            </h6>
                                            <div class="media-body">
                                                <h6 class="my-0 text-secondary font-weight-bold">Koordinat Pelanggan</h6>
                                                <p class="mb-0 text-secondary">
                                                    {{ $data->KOORDINAT }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                
                                        <div class="media" style="margin-top: 1rem;">
                                            <h6 class="mr-2">
                                                <i class="fa fa-map text-secondary"></i>
                                            </h6>
                                            <div class="media-body">
                                                <h6 class="my-0 text-secondary font-weight-bold">Alamat Pelanggan</h6>
                                                <p class="mb-0 text-secondary">
                                                    {{ $data->ALAMAT }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>

                                        <div class="media" style="margin-top: 1rem;">
                                            <h6 class="mr-2">
                                                <i class="fa fa-briefcase text-secondary"></i>
                                            </h6>
                                            <div class="media-body">
                                                <h6 class="my-0 text-secondary font-weight-bold">G-PON</h6>
                                                <p class="mb-0 text-secondary">
                                                    {{ $data->GPON }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                        
                                    </div>
                                       <div class="col-md-6">

                                        <div class="media" style="margin-top: 1rem;">
                                            <h6 class="mr-2">
                                                <i class="fa fa-phone-square text-secondary"></i>
                                            </h6>
                                            <div class="media-body">
                                                <h6 class="my-0 text-secondary font-weight-bold">Telepon Pelanggan</h6>
                                                <p class="mb-0 text-secondary">
                                                +62  {{ $data->NO_HP }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-barcode text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">KODE SALES</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->KODE_SALES  ?? '-'}}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-user text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">NAMA SALES</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->NAMA_SALES ?? '-' }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-map text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">STATUS</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->STATUS ?? '-' }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-cog text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">PORT</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->PORT }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            
                                            
                                            
                                           
                                        </div>
                                        <div class="col-md-6 collapse more-bio">
                                            {{-- Province --}}
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-briefcase text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">SITAC</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->SITAC }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-users text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">P I C</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->PIC }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-expand text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">CAT</h6>
                                                    <p class="mb-0 text-white badge m-0 badge-sm badge-success">
                                                        {{ $data->CAT }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-briefcase text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">SITAC</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->SITAC }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 collapse more-bio">
                                            {{-- Province --}}
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-suitcase text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">SN MODEM DAN AP</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->SN_MODEM_DAN_AP ?? '-' }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-wifi text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">S S I D</h6>
                                                    <p class="mb-0 text-secondary">
                                                        {{ $data->SSID }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h6 class="mr-2">
                                                    <i class="fa fa-expand text-secondary"></i>
                                                </h6>
                                                <div class="media-body">
                                                    <h6 class="my-0 text-secondary font-weight-bold">ODP</h6>
                                                    <p class="mb-0 text-white badge m-0 badge-sm badge-success">
                                                        {{ $data->ODP ?? '-' }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                        </div>
                                
                            </div>
                        </div>
                        <div class="card-footer p-0">
                            <button 
                                class="btn-expand btn btn-light btn-md w-100 rounded-bottom-20"
                                data-toggle="collapse" 
                                data-target=".more-bio" 
                                aria-expanded="false"    
                            >
                                <i class="fa fa-angle-up"></i>
                                <i class="fa fa-angle-down"></i>
                            </button>
                        </div>
                    </a>
               
@endsection
