@extends('layouts.auth')

@section('page')
<div class="container d-flex col-12 offset-lg-7 col-lg-4 col-md-6">
    <div class="col-12 m-auto">
        <div class="card shadow">
            <div class="card-header text-center">
                <img src="{{ asset('images/mywifi.png') }}" alt="" class="img-fluid p-2 mb-2">
                <h4 class="mb-0">
                    {{ config('app.e', 'Sign In') }}
                </h1>
            </div>
            <div class="card-body p-5">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="name">{{ __('E-Mail / Nama Pengguna') }}</label>

                        <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="agung / lisasandi">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <label for="password">{{ __('Sandi') }}</label>

                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="******">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group row">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Ingat Saya') }}
                            </label>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <button type="submit" class="btn bg-purple-old text-light btn-md w-100">
                            {{ __('Masuk') }}
                        </button>

                        {{-- @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
