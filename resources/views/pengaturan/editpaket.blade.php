@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-2">
                <h1 class="p-2 text-purple-old">Edit Data Paket (WMS)</h1>
                <hr class="my-0">
            </div>
        </div>
        @if(\Session::has('success'))
        <div class="alert alert-success">
        <p> {{ \Session::get('success') }}
        </div>
        @endif
        {{-- Filter & Search --}}
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card" style="border-radius: 2rem;">
            <div class="card-header text-white bg-purple-old text-center" style="
            border-radius: 2rem 2rem 0rem 0rem;font-size: 18px;">
           <i class="fa fa-pencil"></i>   Ubah Data Paket
            </div>
            <div class="card-body">
                <form action="{{ route('dashboard.pengaturan.paketupdate',  ['id' => $paket->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        @method('POST')

                    <div class="form-group">
                        <label>Nama Paket</label>
                    <input type="text" class="form-control" name="nama_paket" placeholder="Nama Paket" value="{{ $paket->nama_paket }}">
                          
                        </div>
                        <div class="form-group">
                          <label>Deskripsi Paket</label>
                        <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi Paket" value="{{ $paket->deskripsi }}">
                        </div>
                        
                        <div class="form-group">
                          <label>Harga Paket</label>
                          <input type="text" class="form-control" name="harga_paket" placeholder="Harga Paket" value="{{ $paket->harga_paket }}">
                        </div>
                        
                    
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block"><i class="fa fa-floppy-o"></i> Simpan</button>
            </div>
          </div>
           
</div>

@endsection
