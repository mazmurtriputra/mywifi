
@extends('layouts.app')

@section('content')


{{--List User --}}

<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h1 class="p-2 text-purple-old"><i class="fa fa-list"></i>Daftar SALES</h1>
            <hr class="my-0">
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-4 col-lg-3 mb-4">
            <button class="btn bg-purple-old shadow text-light" data-toggle="modal" data-target="#create-user" role="button" aria-disabled="false"><i class="fa fa-plus"></i> Tambah Sales </button>            
            </div>
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
                  <li class="breadcrumb-item"><a href="/pengaturan">pengaturan</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Laporan</li>
                </ol>
              </nav>
              @if(\Session::has('success'))
              <div class="alert alert-success">
              <p> {{ \Session::get('success') }}
              </div>
              @endif
            <div class="card shadow mb-5 pb-0">
                <div class="dashboard card-body">
                    <div class="list-person row">
                        {{-- List MCU --}}
                        <div class="col-12">
                            <table class="table table-hover table-borderless table-responsive-sm mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-left" width="25%" style="font-size: 18px;">Kode Sales</th>
                                        <th class="text-left" width="25%" style="font-size: 18px;">Nama Sales</th>
                                        <th class="text-left" width="25%" style="font-size: 18px;">Wilayah Sales</th>
                                        <th class="text-left" width="25%" style="font-size: 18px;">Target</th>
                                        <th class="text-left" width="15%" style="font-size: 18px;">Tercapai</th>
                                        <th class="text-left" width="15%" style="font-size: 18px;">Status</th>
                                        <th class="text-left" width="10%" style="font-size: 18px;">Action</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                       @foreach ($sales as $item)
                                        
                                        <tr class="border-top">
                                            <td style="font-size: 18px;">
                                                <h4 class="text-default mb-0">{{ $item->kode_sales ?? '-' }}</h4>                                        
                                            </td>
                                            <td class="patient">
                                                <h4 class="text-default mb-0">{{ $item->nama_sales ?? '-' }}</h4>   
                                            </td>
                                            <td class="patient">
                                                <h4 class="text-default mb-0">{{ $item->wilayah ?? '-' }}</h4>   
                                            </td>
                                            <td class="patient">
                                                <h4 class="text-default mb-0">{{ $item->target ?? '-' }}</h4>   
                                            </td>
                                            <td class="patient">
                                                <h4 class="text-default mb-0">{{  $item->wms->count() ?? '-' }}</h4>   
                                            
                                            </td>
                                            <td class="patient">
                                               <h4> <span class="badge badge-success">{{ $item->status }}</span> </h4>
                                            </td>
                                            {{-- <td style="vertical-align:middle">{{ Carbon\Carbon::now()->formatLocalized('%d %B %Y') }}</td>
                                            <td class="position-relative" style="vertical-align:middle">
                                                <span class="badge badge-success">Aktif</span>
                                            </td> --}}
                                          
                                            <td class="position-relative align-middle">
                                                    
                                                <ul class="navbar-nav text-center">
                                                    <li class="nav-item m-auto">
                                                        <a class="nav-link" href="#" id="member-{{ $loop->index }}" role="button"
                                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-2x fa-ellipsis-v" style="color: red;"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right"
                                                            aria-labelledby="member-{{ $loop->index }}">
                                                           
                                                            <a 
                                                                  href="{{ route('dashboard.pengaturan.salesdetail', ['id' => $item->id]) }}"
                                                                class="dropdown-item"
                                                                style="cursor: pointer;">
                                                                <i class="fa fa-2x fa-eye text-success"></i> Detail 
                                                            </a>

                                                            <a 
                                                            href="{{ route('dashboard.pengaturan.salesdelete',  ['id' => $item->id]) }}" onclick="return confirm('Apakah yakin akan dihapus ?')"
                                                            class="dropdown-item"
                                                            style="cursor: pointer;">
                                                            <i class="fa fa-2x fa-trash text-danger"></i> Hapus
                                                              </a>
                                                    
                                                        </div>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
        
            
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h5 class="modal-title text-light" id="create-patient-label">Tambah Sales</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesstore')  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                          <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                               <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga">
                               
                             </div>
                             <div class="form-group">
                                <strong> <label>Target Sales</label> </strong>
                                   <input type="text" class="form-control" name="target" placeholder="Jumlah Target Sales, contoh: 100">
                                   
                                 </div>

                                 <div class="form-group">
                                    <strong> <label>No HP Sales</label> </strong>
                                       <input type="text" class="form-control" name="no_hp" placeholder="No HP Sales, contoh 0821-xxxx-xxx">
                                       
                                     </div>
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="edit-sales" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h5 class="modal-title text-light" id="create-patient-label">Tambah Sales</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesupdate',  ['id' => $item->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                    <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001" value="{{ $item->kode_sales }}">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                               <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga" value="{{ $item->nama_sales }}">
                               
                             </div>
                            <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="update-sales" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h4 class="modal-title text-light" id="create-patient-label"><i class="fa fa-pencil"></i>Edit Sales</h4>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesupdate',  ['id' => $item->id ?? '-' ])  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                    <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001" value="{{ $item->kode_sales ?? '-' }}">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                        <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga" value="{{ $item->nama_sales ?? '-' }}">
                               
                             </div>

                             <div class="form-group">
                                <strong> <label>Target Sales</label> </strong>
                            <input type="text" class="form-control" name="target" placeholder="Nama Sales, contoh : Angga" value="{{ $item->target ?? '-' }}">
                                   
                                 </div>

                                 <div class="form-group">
                                    <strong> <label>No HP Sales</label> </strong>
                                <input type="text" class="form-control" name="no_hp" placeholder="Nama Sales, contoh : Angga" value="{{ $item->no_hp ?? '-' }}">
                                       
                                     </div>
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection


