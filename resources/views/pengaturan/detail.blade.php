
@extends('layouts.app')

@section('content')


{{--List User --}}

<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Detail Kinerja Sales</h3>
            <hr class="my-0">
        </div>
    </div>

   
    <div class="row">
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
                  <li class="breadcrumb-item"><a href="/pengaturan">Pengaturan</a></li>
                  <li class="breadcrumb-item"><a href="/pengaturan/sales">List Sales</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Detail Sales</li>
                </ol>
              </nav>
              @if(\Session::has('success'))
              <div class="alert alert-success">
              <p> {{ \Session::get('success') }}
              </div>
              @endif
            <div class="card shadow mb-5 pb-0">
                <div class="dashboard card-body">

                    {{-- Pasien --}}
                    <div class="report-group mb-5">
                       <strong> <h1 class="text-purple-old">
                                               
                                                # {{ $sales->kode_sales }}  |  {{ $sales->nama_sales }}  |  <span class="badge badge-success">{{ $sales->status }}</span>
                        </h1> </strong>
                        <hr class="mt-1">
                        {{-- list report --}}
                        <div class="row">
                            <div class="col-sm-3">
                              <div class="card">
                                <div class="card-body">
                                    <i class="fa fa-bullseye"></i>  TARGET :  {{ $sales->target ?? '-' }} 
                                    <i class="fa fa-user"></i>  TERCAPAI :  {{ $sales->wms->count() }} 
                                  <button class="btn bg-purple-old btn-lg btn-block shadow text-light" data-toggle="modal" data-target="#update-sales" role="button" aria-disabled="false"><i class="fa fa-pencil"></i> Edit Data</button>   
                                </div>
                              </div>
                            </div>
                            <div class="col-sm-8">
                              <div class="card">
                                <div class="card-body">
                                  <h5 class="card-title">Special title treatment</h5>
                                  <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                                  <a href="#" class="btn btn-primary">Go somewhere</a>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                    
                    {{-- Doctor --}}
        
            
                </div>
            </div>
           
        </div>
    </div>
</div>
<div class="modal fade" id="update-sales" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h5 class="modal-title text-light" id="create-patient-label">Edit Sales</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesupdate',  ['id' => $sales->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                    <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001" value="{{ $sales->kode_sales }}">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                        <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga" value="{{ $sales->nama_sales }}">
                               
                             </div>

                             <div class="form-group">
                                <strong> <label>Target Sales</label> </strong>
                            <input type="text" class="form-control" name="target" placeholder="Nama Sales, contoh : Angga" value="{{ $sales->target ?? '-' }}">
                                   
                                 </div>

                                 <div class="form-group">
                                    <strong> <label>No HP Sales</label> </strong>
                                <input type="text" class="form-control" name="no_hp" placeholder="Nama Sales, contoh : Angga" value="{{ $sales->no_hp ?? '-' }}">
                                       
                                     </div> 
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


