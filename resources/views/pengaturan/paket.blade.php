
@extends('layouts.app')

@section('content')


{{--List User --}}

<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h1 class="p-2 text-purple-old"><i class="fa fa-globe"></i> DAFTAR PAKET LAYANAN</h1>
            <hr class="my-0">
        </div>
    </div>

    
    <div class="row">
        <div class="col-md-4 col-lg-3 mb-4">
            <button class="btn bg-purple-old shadow text-light" data-toggle="modal" data-target="#create-user" role="button" aria-disabled="false" style="font-size: 16px;"><i class="fa fa-plus"></i> Tambah Paket </button>            
            </div>
        <div class="col-lg-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item" style="font-size: 18px;"><a href="/home">Beranda</a></li>
                  <li class="breadcrumb-item" style="font-size: 18px;"><a href="/pengaturan">Pengaturan</a></li>
                  <li class="breadcrumb-item active" style="font-size: 18px;" aria-current="page">Paket</li>
                </ol>
              </nav>
              @if(\Session::has('success'))
              <div class="alert alert-success">
              <h2> {{ \Session::get('success') }}
              </div>
              @endif
            <div class="card shadow mb-5 pb-0">
                <div class="dashboard card-body">
                    <div class="list-person row">
                        {{-- List MCU --}}
                        <div class="col-12">
                            <table class="table table-hover table-borderless table-responsive-sm mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-center" width="25%" style="font-size: 18px">Nama Paket</th>
                                        <th class="text-center" width="25%" style="font-size: 18px">Deskripsi</th>
                                        <th class="text-center" width="25%" style="font-size: 18px">Harga Paket (Rp.)</th>
                                        <th class="text-center" width="25%" style="font-size: 18px">Aksi</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                       @foreach ($paket as $item)
                                        
                                        <tr class="border-top">
                                            <td style="vertical-align:middle">
                                                <h4 class="text-default text-center">{{ $item->nama_paket ?? '-' }}</h4>                                        
                                            </td>
                                            <td class="patient text-center" style="vertical-align:middle">
                                                <h4 class="text-default text-center">{{ $item->deskripsi ?? '-' }}</h4>                                        
                                            </td>
                                            <td class="patient text-center" style="vertical-align:middle">
                                             <h4 class="text-default text-center">{{ $item->harga_paket ?? '-' }}</h4>         
                                            </td>

                                            <td class="text-center"> 
                                                <a class="btn btn-primary" href="{{ route('dashboard.pengaturan.paketedit',  ['id' => $item->id]) }}" role="button">Edit Paket</a>
                                                <a class="btn btn-primary" href="{{ route('dashboard.pengaturan.paketdelete',  ['id' => $item->id]) }}" onclick="return confirm('Apakah yakin akan dihapus Paket ini ?')" role="button">Hapus Paket</a>
                                            </td>
                                            
                                          
                                           
                                            {{-- <td style="vertical-align:middle">{{ Carbon\Carbon::now()->formatLocalized('%d %B %Y') }}</td>
                                            <td class="position-relative" style="vertical-align:middle">
                                                <span class="badge badge-success">Aktif</span>
                                            </td> --}}
                                          
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        </div>
                    </div>
        
            
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h3 class="modal-title text-light" id="create-patient-label"><i class="fa fa-globe"></i> Tambah Paket</h3>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.paketstore')  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                       <strong> <label>Nama Paket</label> </strong>
                          <input type="text" class="form-control" name="nama_paket" placeholder="Nama Paket internet">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Deskripsi Paket</label> </strong>
                               <input type="text" class="form-control" name="deskripsi" placeholder="Deskripsi dari paket">
                               
                             </div>
                             <div class="form-group">
                                <strong> <label>Harga Paket</label> </strong>
                                   <input type="text" class="form-control" name="harga_paket" placeholder="Harga Paket (RP)">
                                   
                                 </div>
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="edit-sales" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h5 class="modal-title text-light" id="create-patient-label">Tambah Sales</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesupdate',  ['id' => $item->id]) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                    <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001" value="{{ $item->kode_sales }}">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                               <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga" value="{{ $item->nama_sales }}">
                               
                             </div>
                            <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="modal fade" id="update-sales" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h4 class="modal-title text-light" id="create-patient-label"><i class="fa fa-pencil"></i>Edit Sales</h4>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.pengaturan.salesupdate',  ['id' => $item->id ?? '-' ])  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="status">
                        <option>AKTIF</option>
                        <option>TIDAK AKTIF</option>
                        <option>RESIGN</option>

                      </select>
                    </div>    



                    <div class="form-group">
                       <strong> <label>Kode Sales</label> </strong>
                    <input type="text" class="form-control" name="kode_sales" placeholder="Kode Sales, contoh: SALES001" value="{{ $item->kode_sales ?? '-' }}">
                          
                        </div>

                        <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                        <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales, contoh : Angga" value="{{ $item->nama_sales ?? '-' }}">
                               
                             </div>

                             <div class="form-group">
                                <strong> <label>Target Sales</label> </strong>
                            <input type="text" class="form-control" name="target" placeholder="Nama Sales, contoh : Angga" value="{{ $item->target ?? '-' }}">
                                   
                                 </div>

                                 <div class="form-group">
                                    <strong> <label>No HP Sales</label> </strong>
                                <input type="text" class="form-control" name="no_hp" placeholder="Nama Sales, contoh : Angga" value="{{ $item->no_hp ?? '-' }}">
                                       
                                     </div>
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div> --}}
@endsection


