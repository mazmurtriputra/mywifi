@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-2">
                <h3 class="p-2 text-purple-old">Edit Data Pelanggan (WMS)</h3>
                <hr class="my-0">
            </div>
        </div>
        @if(\Session::has('success'))
        <div class="alert alert-success">
        <p> {{ \Session::get('success') }}
        </div>
        @endif
        {{-- Filter & Search --}}
        <div class="row">
            <div class="col-md-4 col-lg-3 mb-4">
                <a href="{{ route('dashboard.registrasi.prospek') }}" class="btn bg-purple-old shadow text-light" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali </a>
            </div>
          
            <form method="GET" class="col-md-8 col-lg-9">
                <div class="row">
                    <div class="col-md-6 col-lg-7 mb-4">
                        <div class="card shadow filter-field border-0">
                            <div class="form-row align-items-center">
                                <div class="field col-md-4 p-0">
                                    <select name="jenis-kelamin" class="custom-select border-0">
                                        <option value="semua" selected>Nama</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="otoritas" class="custom-select border-0">
                                        <option value="semua" selected>Jenis</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="status" class="custom-select border-0">
                                        <option value="semua" selected>Status</option>
                                        <option value="aktif" {{ request('status') == 'aktif' ? 'selected' : '' }}>Lunas</option>
                                        <option value="tidak-aktif" {{ request('status') == 'tidak-aktif' ? 'selected' : '' }}>Belum Lunas</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5 mb-4">
                        <div class="card shadow search-field border-0">
                            <div class="form-row align-items-center">
                                <div class="col-10 p-0">
                                    <input type="text" name="pencarian" value="{{ request('pencarian') }}" class="form-control border-0" id="inlineFormInput" placeholder="Pencarian">
                                </div>
                                <div class="col-2 p-0">
                                    <button type="submit" class="btn w-100 bg-purple-old text-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card" style="border-radius: 2rem;">
            <div class="card-header text-white bg-purple-old" style="
            border-radius: 2rem 2rem 0rem 0rem;">
              Ubah Data WMS
            </div>
            <div class="card-body">
                <form action="{{ route('dashboard.registrasi.wmsupdate',  ['id' => $wms->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        @method('POST')
                        <div class="form-group">
                          <label>STATUS</label>
                          <select class="form-control" name="cust_status">
                            <option>LANGGANAN</option>
                          </select>
                        </div>
                    <div class="form-group">
                        <label>Nama Pelanggan</label>
                    <input type="text" class="form-control" name="nama_plg" placeholder="Nama Pelanggan" value="{{ $wms->nama_plg }}">
                          
                        </div>
                        <div class="form-group">
                          <label>Alamat Pelanggan</label>
                        <input type="text" class="form-control" name="alamat" placeholder="Alamat pelanggan" value="{{ $wms->alamat }}">
                        </div>
                        
                        <div class="form-group">
                          <label>Nomor HP Pelanggan</label>
                          <input type="text" class="form-control" name="no_hp" placeholder="Nomor HP Pelanggan" value="{{ $wms->no_hp }}">
                        </div>
                        <div class="form-group">
                            <label>Email Pelanggan</label>
                            <input type="text" class="form-control" name="email" placeholder="Email Pelanggan" value="{{ $wms->email }}">
                          </div>
                          <div class="form-group">
                            <label>Kecepatan </label>
                            <input type="text" class="form-control" name="kecepatan" placeholder="Kecepatan Paket" value="{{ $wms->kecepatan }}">
                          </div>
                          <div class="form-group">
                            <label>Layanan</label>
                            <input type="text" class="form-control" name="layanan" placeholder="Layanan" value="{{ $wms->layanan }}">
                          </div>
                          <div class="form-group">
                            <label>Jumlah AP</label>
                            <input type="text" class="form-control" name="jml_ap" placeholder="Jumlah Access Point" value="{{ $wms->jml_ap }}">
                          </div>
                        
                          <div class="form-group">
                            <label>Biaya Perbulan</label>
                            <input type="text" class="form-control" name="biaya_perbulan" placeholder="Biaya Perbulan" value="{{ $wms->biaya_perbulan }}">
                          </div>

                          <div class="form-group">
                            <label>P P N</label>
                            <input type="text" class="form-control" name="ppn" placeholder="P P N" value="{{ $wms->ppn }}">
                          </div>

                          <div class="form-group">
                            <label>Total</label>
                            <input type="text" class="form-control" name="total" placeholder="Total" value="{{ $wms->total }}">
                          </div>

                          <div class="form-group">
                            <label>SSID</label>
                            <input type="text" class="form-control" name="ssid" placeholder="SSID" value="{{ $wms->ssid }}">
                          </div>
                          <div class="form-group">
                            <label>Password</label>
                            <input type="text" class="form-control" name="password" placeholder="Password" value="{{ $wms->password }}">
                          </div>
    
                        {{-- <div class="form-group">
                            <b>File Gambar Rumah</b><br/>
                            <input type="file" name="image">
                        </div> --}}

                        <div class="form-group">
                            <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH</label> </strong>
                            <div class="input-group control-group increment" >
                            <input type="file" name="foto_pelanggan[]" multiple class="form-control" value="{{ $wms->foto_pelanggan }}">
                            </div>
                          </div>
                    
                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block"><i class="fa fa-floppy-o"></i> Simpan</button>
            </div>
          </div>
           
</div>

@endsection
