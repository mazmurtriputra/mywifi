@extends('layouts.app')

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Beranda</a></li>
        </ol>
      </nav>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
               
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.registrasi.prospek')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Prospek Pelanggan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-user-plus px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Registrasi Prospek</h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xl-6 col-md-6 col-12 mb-4">
                    <a href="{{ route('dashboard.registrasi.wms')  }}" class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2" style="
                        background-color: red;
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Berlangganan
                            </h5>
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="media">
                                <h1 class="mr-2 d-flex">
                                    <i class="fa fa-1x fa-user-plus px-4 py-4 m-auto text-dark"></i>
                                </h1>
                                <div class="media-body my-auto">
                                    <h2 class="my-0 text-secondary">Registrasi WMS  </h2>
                                    <hr class="mt-0 mb-2">
                                    {{-- <h5 class="text-dark mb-0">
                                         Pasien
                                    </h5> --}}
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
        </div>
    </div>
</div>
@endsection
