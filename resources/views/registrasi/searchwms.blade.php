@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-2">
                <h3 class="p-2 text-purple-old">Data WMS</h3>
                <hr class="my-0">
            </div>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
              <li class="breadcrumb-item"><a href="/registrasi">Registrasi</a></li>
              <li class="breadcrumb-item active" aria-current="page">Wms    </li>
            </ol>
          </nav>
        
        {{-- Filter & Search --}}
        <div class="row">
            <div class="col-md-4 col-lg-3 mb-4">
            <button class="btn bg-purple-old shadow text-light" data-toggle="modal" data-target="#create-user" role="button" aria-disabled="false"><i class="fa fa-plus"></i> Registrasi WMS </button>            
        </div>
            <form method="GET" class="col-md-8 col-lg-9">
                <div class="row">
                    <div class="col-md-6 col-lg-7 mb-4">
                        <div class="card shadow filter-field border-0">
                            <div class="form-row align-items-center">
                                <div class="field col-md-4 p-0">
                                    <select name="jenis-kelamin" class="custom-select border-0">
                                        <option value="semua" selected>Nama</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="otoritas" class="custom-select border-0">
                                        <option value="semua" selected>Jenis</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="status" class="custom-select border-0">
                                        <option value="semua" selected>Status</option>
                                        <option value="aktif" {{ request('status') == 'aktif' ? 'selected' : '' }}>Prospek</option>
                                        <option value="tidak-aktif" {{ request('status') == 'tidak-aktif' ? 'selected' : '' }}>Belum Lunas</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5 mb-4">
                        <div class="card shadow search-field border-0">
                            <div class="form-row align-items-center">
                                <form action="{{ route('wmssearch') }}" class="form-inline">
                                <div class="col-10 p-0">
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" class="form-control border-0" id="inlineFormInput" placeholder="Pencarian">
                                </div>
                                <div class="col-2 p-0">
                                    <button type="submit" class="btn w-100 bg-purple-old text-light"><i class="fa fa-search"></i></button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-5 pb-0">
                    <div class="patient card-body">
                        @if(\Session::has('success'))
                        <div class="alert alert-success">
                        <p> {{ \Session::get('success') }}
                        </div>
                        @endif
                        <div class="list-person row">
                           
                            <div class="col-12">
                                <table class="table table-hover table-borderless table-responsive-sm mb-0">
                                    <thead>
                                        <tr>
                                            <th class="text-left" width="25%">Nama Pelanggan</th>
                                            <th class="text-left" width="25%">Alamat</th>
                                            <th class="text-left" width="25%">No HP</th>
                                            <th class="text-left">Email</th>
                                            <th class="text-left" width="10%">Status</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            @foreach ($data as $item)
                                                
                                            <tr class="border-top">
                                                <td style="vertical-align:middle">
                                                <h6 class="text-default mb-0">{{ $item->nama_plg }}</h6>                                        
                                                </td>
                                                
                                                <td class="patient">
                                                    {{ $item->alamat }}
                                                </td>
                                                <td style="vertical-align:middle"> {{ $item->no_hp }}
                                                </td>
                                                <td class="position-relative" style="vertical-align:middle">
                                                    {{ $item->email }}
                                                </td>
                                                <td class="patient">
                                                    <span class="badge badge-success">{{ $item->cust_status }}</span> 
                                                </td>
                                                <td class="position-relative align-middle">
                                                    
                                                    <ul class="navbar-nav text-center">
                                                        <li class="nav-item m-auto">
                                                            <a class="nav-link" href="#" id="member-{{ $loop->index }}" role="button"
                                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-v" style="color: red;"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right"
                                                                aria-labelledby="member-{{ $loop->index }}">
                                                                <a 
                                                                    href="{{ route('dashboard.registrasi.wmsedit',  ['id' => $item->id]) }}"
                                                                    class="dropdown-item"
                                                                    style="cursor: pointer;">
                                                                    <i class="fa fa-pencil text-default"></i> Ubah
                                                                </a>
                                                                <a 
                                                                      href="{{ route('dashboard.registrasi.profile', ['id' => $item->id]) }}"
                                                                    class="dropdown-item"
                                                                    style="cursor: pointer;">
                                                                    <i class="fa fa-eye text-success"></i> Detail
                                                                </a>

                                                                <a 
                                                                href="{{ route('dashboard.registrasi.wmsdelete',  ['id' => $item->id]) }}" onclick="return confirm('Apakah yakin akan dihapus ?')"
                                                                class="dropdown-item"
                                                                style="cursor: pointer;">
                                                                <i class="fa fa-trash text-danger"></i> Hapus
                                                                  </a>
                                                        
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                </table>
                                {{ $data->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h5 class="modal-title text-light" id="create-patient-label">Registrasi WMS</h5>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.registrasi.wmsstore')  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="cust_status">
                        <option>LANGGANAN</option>
                      </select>
                    </div>    

                    <div class="form-group">
                      <strong>  <label>Tanggal Registrasi</label> </strong>
                        <input type="date" class="form-control" name="tgl_regis">
                      </div>

                    <div class="form-group">
                       <strong> <label>Nama Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="nama_plg" placeholder="Nama Pelanggan">
                          
                        </div>
                        <div class="form-group">
                         <strong> <label>Alamat Pemasangan</label></strong>
                          <input type="text" class="form-control" name="alamat" placeholder="Alamat pelanggan">
                        </div>
                        
                        <div class="form-group">
                          <strong> <label>Nomor HP Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="no_hp" placeholder="Nomor HP Pelanggan">
                        </div>

                        <div class="form-group">
                           <strong> <label>Email Pelanggan</label> </strong>
                            <input type="text" class="form-control" name="email" placeholder="Email Pelanggan">
                          </div>
                          <div class="form-group">
                           <strong> <label> Layanan</label> </strong>
                            <input type="text" class="form-control" name="layanan" placeholder="Detail Layanan Paket">
                          </div>
                          <div class="form-group">
                           <strong> <label>Kecepatan</label> </strong>
                            <input type="text" class="form-control" name="kecepatan" placeholder="Kecepatan paket">
                          </div>
                          <div class="form-group">
                           <strong> <label>AP (Access Point)</label> </strong>
                            <input type="text" class="form-control" name="jml_ap" placeholder="Jumlah AP">
                          </div>
                          <div class="form-group">
                           <strong> <label>Biaya Bulanan</label></strong>
                            <input type="text" class="form-control" name="biaya_perbulan" placeholder="Biaya Bulanan">
                          </div>
                          <div class="form-group">
                           <strong> <label>PPN</label></strong>
                            <input type="text" class="form-control" name="ppn" placeholder="PPN">
                          </div>
                          <div class="form-group">
                           <strong> <label>Total Biaya</label> </strong>
                            <input type="text" class="form-control" name="total" placeholder="Total Biaya">
                          </div>
                          <div class="form-group">
                            <strong> <label>SSID</label> </strong>
                            <input type="text" class="form-control" name="ssid" placeholder="SSID">
                          </div>
                          <div class="form-group">
                           <strong> <label>Password</label> </strong>
                            <input type="text" class="form-control" name="password" placeholder="Password">
                          </div>

                          <div class="form-group">
                            <strong> <label>Kode Sales</label> </strong>
                             <input type="text" class="form-control" name="kode_sales" placeholder="Nomor Kode Sales">
                           </div>

                           <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                             <input type="text" class="form-control" name="nama_sales" placeholder="Nama Sales">
                           </div>
                          
                          <div class="form-group">
                            <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH</label> </strong>
                            <div class="input-group control-group increment" >
                              <input type="file" name="foto_pelanggan[]" multiple class="form-control">
                            </div>
                          </div>
                          <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>
<script>


    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>
@endsection