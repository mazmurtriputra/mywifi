@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Data Detail Pelanggan (WMS)</h3>
            <hr class="my-0">
        </div>
    </div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
              <li class="breadcrumb-item"><a href="/registrasi">Registrasi</a></li>
              <li class="breadcrumb-item"><a href="/registrasi/wms">List data WMS</a></li>
              <li class="breadcrumb-item active" aria-current="page">Detail</li>
            </ol>
          </nav>
                     @if(\Session::has('success'))
                        <div class="alert alert-success">
                        <p> {{ \Session::get('success') }}
                        </div>
                        @endif
                      <a class="dashboard card shadow" style="border-radius: 1rem;">
                        <div class="card-header bg-purple-old-fade py-2 bg-purple-old" style="
                        border-radius: 1rem 1rem 0rem 0rem;">
                            <h5 class="text-white mb-0">
                                Profil Pelanggan
                            </h5>
                            
                        </div>
                        <div class="card-body">
                            {{-- Amount --}}
                            <div class="bio row mb-4">
                         
                                    <div class="col-md-6">
                                        <div class="media">
                                            <img src="{{ asset('images/user.png') }}" alt="..." class="img-thumbnail rounded-circle mr-1" style="height: 100px">
                                        
                                            <div class="media-body" style="margin-top: 2rem;margin-left: 1rem;">
                                                <h3 class="my-0 text-secondary font-weight-bold">NAMA LENGKAP</h3>
                                                <h3 class="mb-0 text-secondary">
                                                    {{ $data->nama_plg }}
                                                </h3>
                                                <hr class="mt-0 mb-2">  
                                            </div>
                                        </div>

                                        {{-- <div class="media" style="margin-top: 1rem;">
                                            <h5 class="mr-2">
                                                <i class="fa fa-chrome text-secondary"></i>
                                            </h5>
                                            <div class="media-body">
                                                <h5 class="my-0 text-secondary font-weight-bold">Nomor Internet</h5>
                                                <p class="mb-0 text-secondary">
                                                    {{ $data->INETSID ?? '-' }}
                                                </p>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div> --}}
                                        
                                        <div class="media" style="margin-top: 1rem;">
                                            <h5 class="mr-2">
                                                <i class="fa fa-envelope text-secondary"></i>
                                            </h5>
                                            <div class="media-body">
                                                <h3 class="my-0 text-secondary font-weight-bold">EMAIL PELANGGAN</h3>
                                                <h4 class="mb-0 text-secondary">
                                                    {{ $data->email }}
                                                </h4>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                
                                        <div class="media" style="margin-top: 1rem;">
                                            <h5 class="mr-2">
                                                <i class="fa fa-map text-secondary"></i>
                                            </h5>
                                            <div class="media-body">
                                                <h3 class="my-0 text-secondary font-weight-bold">ALAMAT PELANGGAN</h3>
                                                <h4 class="mb-0 text-secondary">
                                                    {{ $data->alamat }}
                                                </h4>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>

                                            {{-- Province --}}
                                            
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-globe text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">NO INTERNET</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->inet ?? '-' }}
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-certificate text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">ODP</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->odp ?? '-' }}
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>

                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-calendar text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">TANGGAL REGISTRASI</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->tgl_regis ?? '-' }}

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            

                                      
                                        
                                    </div>
                                       <div class="col-md-6">

                                        <div class="media" style="margin-top: 1rem;">
                                            <h3 class="mr-2">
                                                <i class="fa fa-phone-square text-secondary"></i>
                                            </h3>
                                            <div class="media-body">
                                                <h3 class="my-0 text-secondary font-weight-bold">Telepon Pelanggan</h3>
                                                <h4 class="mb-0 text-secondary">
                                                +62  {{ $data->no_hp }}
                                                </h4>
                                                <hr class="mt-0 mb-2">
                                            </div>
                                        </div>
                                    
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-briefcase text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">STATUS</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                       <span class="badge badge-success"> {{ $data->cust_status ?? '-' }} </span>
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>

                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-hourglass-start text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">STATUS REGISTRASI</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                       <span class="badge badge-success"> {{ $data->status ?? '-' }} </span>
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>

                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-bandcamp text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">KETERANGAN STATUS</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                       <span class="badge badge-success"> {{ $data->ket ?? '-' }} </span>
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>

                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-chrome text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">LAYANAN</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                        {{ $data->paket->nama_paket ?? '-' }}
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-tachometer text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">KECEPATAN</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                        {{ $data->paket->deskripsi ?? '-' }}
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-link text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">JUMLAH AP</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->jml_ap ?? '-' }}

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            
                                        
                                        </div>
                                       
                                        <div class="col-md-6">
                                            {{-- Province --}}
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-money text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">BIAYA PERBULAN</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                        {{ $data->paket->harga_paket ?? '-' }}
                                                   

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            {{-- <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-percent text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">P P N</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->ppn ?? '-' }}

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-columns text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">TOTAL</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->total ?? '-' }}

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div> --}}

                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-wifi text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">S S I D</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->ssid ?? '-' }}
                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-key text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">PASSWORD</h3>
                                                    <p class="mb-0 text-secondary">
                                                    {{ $data->password ?? '-' }}
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>

                                           

                                       
                                           @php
                                               $items = json_decode($data->foto_pelanggan, true);
                                           @endphp
                                        </div>
                                        <div class="col-md-6">
                                            {{-- Province --}}
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-code text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">KODE SALES</h3>
                                                    <p class="mb-0 text-secondary">
                                                    {{ $data->sales->kode_sales ?? '-' }}

                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                </div>
                                            </div>
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-user text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">NAMA SALES / WILAYAH</h3>
                                                    <h4 class="mb-0 text-secondary">
                                                    {{ $data->sales->nama_sales ?? '-' }} / {{ $data->sales->wilayah ?? '-' }}

                                                    </h4>
                                                    <hr class="mt-0 mb-2">
                                       <button class="btn bg-purple-old btn-lg btn-block shadow text-light" data-toggle="modal" data-target="#update-wms" role="button" aria-disabled="false"><i class="fa fa-2x fa-pencil"></i> Edit Data</button>   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            {{-- Province --}}
                                            <div class="media" style="margin-top: 1rem;">
                                                <h3 class="mr-2">
                                                    <i class="fa fa-picture-o text-secondary"></i>
                                                </h3>
                                                <div class="media-body">
                                                    <h3 class="my-0 text-secondary font-weight-bold">FOTO PELANGGAN</h3>
                                                    <p class="mb-0 text-secondary">   
                                                    </p>
                                                    <hr class="mt-0 mb-2">
                                                    <i> <h4 class="my-0 text-secondary font-weight-bold">*Foto wajib menampilkan foto KTP , KTP dipegang Pelanggan , Rumah , Koordinat ODP , Koordinat Pelanggan.</h4> </i>
                                                </div>
                                                <button class="btn bg-purple-old shadow text-light" data-toggle="modal" data-target="#update-wms-photo" role="button" aria-disabled="false" style="margin-bottom: 2rem;"><i class="fa fa-image"></i> Edit Foto </button>            
                                            </div>
                                            <div class="card-group">
                                                @foreach ($items as $row)
                                                
                                                  <img class="card-img-top" src="{{ asset('/images/'.$row) ?? '-' }}" alt="Card image cap">
                                                
                                                @endforeach
                                              </div>                                              
                                        </div>
                                        
                            </div>      
                         

                        </div>
                        <div class="card-footer p-0">
                            <button 
                                class="btn-expand btn btn-light btn-md w-100 rounded-bottom-20"
                                data-toggle="collapse" 
                                data-target=".more-bio" 
                                aria-expanded="false"    
                            >
                                <i class="fa fa-angle-up"></i>
                                <i class="fa fa-angle-down"></i>
                            </button>
                        </div>
                       
                    </a>
                    <div class="modal fade" id="update-wms" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content border-0">
                            <div class="modal-header bg-purple-old py-2">
                                <h5 class="modal-title text-light" id="create-patient-label">Edit Registrasi WMS</h5>
                                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="{{ route('dashboard.registrasi.wmsupdate',  ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                        @method('POST')
                                        <div class="form-group">
                                          <label>STATUS</label>
                                          <select class="form-control" name="cust_status">
                                            <option>LANGGANAN</option>
                                          </select>
                                        </div>

                                    <div class="form-group">
                                        <label>Nama Pelanggan</label>
                                    <input type="text" class="form-control" name="nama_plg" placeholder="Nama Pelanggan" value="{{ $data->nama_plg }}">
                                          
                                        </div>
                                        <div class="form-group">
                                          <label>Alamat Pelanggan</label>
                                        <input type="text" class="form-control" name="alamat" placeholder="Alamat pelanggan" value="{{ $data->alamat }}">
                                        </div>
                                        
                                        <div class="form-group">
                                          <label>Nomor HP Pelanggan</label>
                                          <input type="text" class="form-control" name="no_hp" placeholder="Nomor HP Pelanggan" value="{{ $data->no_hp }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Email Pelanggan</label>
                                            <input type="text" class="form-control" name="email" placeholder="Email Pelanggan" value="{{ $data->email }}">
                                          </div>
                                          <div class="form-group">
                                            <label>Jumlah AP</label>
                                            <input type="text" class="form-control" name="jml_ap" placeholder="Jumlah Access Point" value="{{ $data->jml_ap }}">
                                          </div>
                                         <div class="form-group">
                                            <label>SSID</label>
                                            <input type="text" class="form-control" name="ssid" placeholder="SSID" value="{{ $data->ssid }}">
                                          </div>
                                          <div class="form-group">
                                            <label>Password</label>
                                            <input type="text" class="form-control" name="password" placeholder="Password" value="{{ $data->password }}">
                                          </div>
                                          <div class="form-group">
                                            <label>No Internet</label>
                                            <input type="text" class="form-control" name="inet" placeholder="No internet" value="{{ $data->inet }}">
                                          </div>
                                          <div class="form-group">
                                            <label>ODP</label>
                                            <input type="text" class="form-control" name="odp" placeholder="Odp pelanggan" value="{{ $data->odp }}">
                                          </div>

                                          <div class="form-group">
                                            <div class="form-group">
                                                <label>Status Registrasi</label>
                                                <select class="form-control" name="status">
                                                    <option>OKE TARIK</option>
                                                  <option>PENDING</option>
                                                </select>
                                              </div>
                                          </div>

                                        
                                          <div class="form-group">
                                            <strong> <label>Keterangan</label> </strong>
                                             <input type="text" class="form-control" name="ket" placeholder="Keterangan dari Status Registrasi, misal ODP FULL" value="{{ $data->ket }}">
                                           </div>
                                          
                    
                                        {{-- <div class="form-group">
                                            <b>File Gambar Rumah</b><br/>
                                            <input type="file" name="image">
                                        </div> --}}
                
                                        {{-- <div class="form-group">
                                            <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH</label> </strong>
                                            <div class="input-group control-group increment" >
                                            <input type="file" name="foto_pelanggan[]" multiple class="form-control" value="{{ $data->foto_pelanggan }}">
                                            </div>
                                          </div> --}}
                                    
                                    <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block"><i class="fa fa-floppy-o"></i> Simpan</button>   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="update-wms-photo" tabindex="-1" aria-labelledby="update-wms-photo" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header bg-purple-old text-light">
                          <h5 class="modal-title" id="update-wms-photo">Update Foto Pelanggan</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                    
                        <div class="modal-body">
                         {{ csrf_field() }}
                         <form action="{{ route('dashboard.registrasi.wmsupdatephoto',  ['id' => $data->id]) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                                @method('POST')

                                <div class="form-group">
                                    <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH , Titik Koordinat</label> </strong>
                                    <div class="input-group control-group increment" >
                                    <input type="file" name="foto_pelanggan[]" multiple class="form-control" value="{{ $data->foto_pelanggan }}">
                                    </div>
                                  </div>
                            
                            <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block"><i class="fa fa-floppy-o"></i> Simpan</button>   
                        </form>
                        </div>
                        <div class="modal-footer">
                        </div>
                      </div>
                    </div>
                </div>
<script>
                    $(function() {
		$('.pop').on('click', function() {
			$('.imagepreview').attr('src', $(this).find('img').attr('src'));
			$('#imagemodal').modal('show');   
		});		
});
</script>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">              
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <img src="" class="imagepreview" style="width: 100%;" >
        </div>
      </div>
    </div>
  </div>

@endsection
