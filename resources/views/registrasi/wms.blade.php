@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-2">
                <h1 class="p-2 text-purple-old"><i class="fa fa-list" style="margin-right: 1rem;"></i>Data WMS</h1>
                <hr class="my-0">
            </div>
        </div>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item" style="font-size: 18px;"><a href="/home">Beranda</a></li>
              <li class="breadcrumb-item" style="font-size: 18px;"><a href="/registrasi">Registrasi</a></li>
              <li class="breadcrumb-item active" aria-current="page" style="font-size: 18px;">Wms    </li>
            </ol>
          </nav>
        
        {{-- Filter & Search --}}
        <div class="row">
            <div class="col-md-4 col-lg-3 mb-4">
            <button class="btn bg-purple-old shadow text-light" data-toggle="modal" data-target="#create-user" role="button" aria-disabled="false" style="font-size: 18px;"><i class="fa fa-plus"></i> Registrasi WMS </button>            
            </div>
            <form action="{{ route('dashboard.registrasi.wmssearch') }}" method="GET" class="col-md-8 col-lg-9">
                <div class="row">
                    <div class="col-md-6 col-lg-7 mb-4">
                        <div class="card shadow filter-field border-0">
                            <div class="form-row align-items-center">
                                <div class="field col-md-4 p-0">
                                    <select name="jenis-kelamin" class="custom-select border-0">
                                        <option value="semua" selected>Nama</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="otoritas" class="custom-select border-0">
                                        <option value="semua" selected>Jenis</option>
                                    </select>
                                </div>
                                <div class="field col-md-4 p-0">
                                    <select name="status" class="custom-select border-0">
                                        <option value="semua" selected>Status</option>
                                        <option value="aktif" {{ request('status') == 'aktif' ? 'selected' : '' }}>Prospek</option>
                                        <option value="tidak-aktif" {{ request('status') == 'tidak-aktif' ? 'selected' : '' }}>Langganan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-5 mb-4">
                        <div class="card shadow search-field border-0">
                            <div class="form-row align-items-center">
                                <div class="col-10 p-0">
                                    <input type="text" name="keyword" value="{{ request('keyword') }}" class="form-control border-0" id="inlineFormInput" placeholder="Pencarian">
                                </div>
                                <div class="col-2 p-0">
                                    <button type="submit" class="btn w-100 bg-purple-old text-light"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="card shadow mb-5 pb-0">
                    <div class="patient card-body">

                        @if ($errors->any())
                        <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                        </div><br/>
                        @endif
                        
                        @if(\Session::has('success'))
                        <div class="alert alert-success">
                        <p> {{ \Session::get('success') }}
                        </div>
                        @endif
                        <div class="list-person row">
                            <div class="col-12">
                                
                                <table class="table table-hover table-borderless table-responsive-sm mb-0">
                                    <thead>
                                        <tr>
                                            <th class="text-left" style="font-size: 18px;" width="25%">Nama Pelanggan</th>
                                            <th class="text-left" style="font-size: 18px;" width="25%">Alamat</th>
                                            <th class="text-left" style="font-size: 18px;" width="25%">No HP</th>
                                            <th class="text-left" style="font-size: 18px;" width="25%">Email</th>
                                            <th class="text-left" style="font-size: 18px;" width="10%">Status Registrasi</th>
                                            <th class="text-left" style="font-size: 18px;" width="10%">Keterangan Status</th>
                                            <th class="text-left" style="font-size: 18px;" width="10%">Status</th>
                                        </tr>
                                    </thead>
                                        <tbody>
                                            @foreach ($wms as $item)
                                                
                                            <tr class="border-top">
                                                <td style="vertical-align:middle">
                                                <h4 class="text-default mb-0">{{ $item->nama_plg }}</h4>                                        
                                                </td>
                                                
                                                <td class="patient" style="vertical-align:middle">
                                                    <h4 class="text-default mb-0">{{ $item->alamat }}</h4>   
                                                </td>
                                                <td style="vertical-align:middle"> 
                                                    <h4 class="text-default mb-0">{{ $item->no_hp }}</h4>   
                                                </td>
                                                <td class="position-relative" style="vertical-align:middle">
                                                    <h4 class="text-default mb-0">{{ $item->email }}</h4>   
                                                </td>
                                                <td class="patient">
                                                    <h3> <span class="badge badge-secondary">{{ $item->status }}</span>  </h3>
                                                </td>
                                                <td class="patient">
                                                    <h3> <span class="badge badge-success">{{ $item->ket }}</span>  </h3>
                                                </td>
                                                <td class="patient">
                                                    <h3> <span class="badge badge-primary">{{ $item->cust_status }}</span>  </h3>
                                                </td>
                                                
                                                <td class="position-relative align-middle">
                                                    
                                                    <ul class="navbar-nav text-center">
                                                        <li class="nav-item m-auto">
                                                            <a class="nav-link" href="#" id="member-{{ $loop->index }}" role="button"
                                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-2x fa-ellipsis-v" style="color: red;"></i>
                                                            </a>
                                                            <div class="dropdown-menu dropdown-menu-right"
                                                                aria-labelledby="member-{{ $loop->index }}">
                                                                {{-- <a 
                                                                    href="{{ route('dashboard.registrasi.wmsedit',  ['id' => $item->id]) }}"
                                                                    class="dropdown-item"
                                                                    style="cursor: pointer;">
                                                                    <i class="fa fa-2x fa-pencil text-default"></i> Ubah
                                                                </a> --}}
                                                                <a 
                                                                      href="{{ route('dashboard.registrasi.profile', ['id' => $item->id]) }}"
                                                                    class="dropdown-item"
                                                                    style="cursor: pointer;">
                                                                    <i class="fa fa-2x fa-eye text-success"></i> Detail
                                                                </a>

                                                                <a 
                                                                href="{{ route('dashboard.registrasi.wmsdelete',  ['id' => $item->id]) }}" onclick="return confirm('Apakah yakin akan dihapus ?')"
                                                                class="dropdown-item"
                                                                style="cursor: pointer;">
                                                                <i class="fa fa-2x fa-trash text-danger"></i> Hapus
                                                                  </a>
                                                        
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                </table>
                                {{ $wms->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="create-patient-label"
    aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header bg-purple-old py-2">
                <h2 class="modal-title text-light" id="create-patient-label"><i class="fa fa-plus"></i>Registrasi WMS</h2>
                <button type="button" class="close text-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('dashboard.registrasi.wmsstore')  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="cust_status">
                        <option>LANGGANAN</option>
                      </select>
                    </div>    

                    <div class="form-group">
                      <strong>  <label>Tanggal Registrasi</label> </strong>
                        <input type="date" class="form-control" name="tgl_regis">
                      </div>

                    <div class="form-group">
                       <strong> <label>Nama Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="nama_plg" placeholder="Nama Pelanggan">
                          
                        </div>
                        <div class="form-group">
                         <strong> <label>Alamat Pemasangan</label></strong>
                          <input type="text" class="form-control" name="alamat" placeholder="Alamat pelanggan">
                        </div>
                        
                        <div class="form-group">
                          <strong> <label>Nomor HP Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="no_hp" placeholder="Nomor HP Pelanggan">
                        </div>
                        <div class="form-group">
                           <strong> <label>Email Pelanggan</label> </strong>
                            <input type="text" class="form-control" name="email" placeholder="Email Pelanggan">
                          </div>


                   <div class="form-group">
                    <strong> <label>Paket</label> </strong>
                   <select name="id_pakets" class="custom-select border-10">
                    @foreach($pakets as $paket)
                        <option value="{{$paket->id}}" {{(isset($department->id) || old('id'))? "selected":"" }}> {{$paket->nama_paket}} - {{$paket->deskripsi}} - {{$paket->harga_paket}} </option> 
                        @endforeach    
                    </select>
                    <a class="btn btn-primary" href="{{ route('dashboard.pengaturan.paket') }}" style="margin-top: 1rem;" role="button"><i class="fa fa-globe"></i> Tambah Paket</a>
                     
                          <div class="form-group">
                           <strong> <label>AP (Access Point)</label> </strong>
                            <input type="text" class="form-control" name="jml_ap" placeholder="Jumlah AP">
                          </div>
                          <div class="form-group">
                            <strong> <label>SSID</label> </strong>
                            <input type="text" class="form-control" name="ssid" placeholder="SSID">
                          </div>
                          <div class="form-group">
                           <strong> <label>Password</label> </strong>
                            <input type="text" class="form-control" name="password" placeholder="Password">
                          </div>

                          <div class="form-group">
                            <strong> <label>No Internet</label> </strong>
                             <input type="text" class="form-control" name="inet" placeholder="No internet pelanggan">
                           </div>

                           <div class="form-group">
                            <strong> <label>ODP</label> </strong>
                             <input type="text" class="form-control" name="odp" placeholder="ODP">
                           </div>

                          <div class="form-group">
                            <div class="form-group">
                                <label>Status Registrasi</label>
                                <select class="form-control" name="status">
                                    <option>OKE TARIK</option>
                                  <option>PENDING</option>
                                </select>
                              </div>
                          </div>
                          <div class="form-group">
                            <strong> <label>Keterangan</label> </strong>
                             <input type="text" class="form-control" name="ket" placeholder="Keterangan dari Status Registrasi, misal ODP FULL">
                           </div>

                           <div class="form-group">
                            <strong> <label>Nama Sales</label> </strong>
                           <select name="id_sales" class="custom-select border-10">
                            @foreach($sales as $sale)
                       <option value="{{$sale->id}}" {{(isset($department->id) || old('id'))? "selected":"" }}> {{$sale->nama_sales}}</option> 
                     @endforeach    
                   </select>
               <a class="btn btn-primary" href="{{ route('dashboard.pengaturan.sales') }}" style="margin-top: 1rem;" role="button"><i class="fa fa-user"></i> Tambah Sales</a>
                           </div>                  
                          <div class="form-group">
                            <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH</label> </strong>
                            <div class="input-group control-group increment" >
                              <input type="file" name="foto_pelanggan[]" multiple class="form-control">
                            </div>
                          </div>
                          <button type="submit" class="btn bg-purple-old shadow text-light btn-lg btn-block" style="border-radius: 3rem;">Simpan</button>    
                </form>
            </div>
        </div>
    </div>
</div>
<script>


    $(document).ready(function() {

      $(".btn-success").click(function(){ 
          var html = $(".clone").html();
          $(".increment").after(html);
      });

      $("body").on("click",".btn-danger",function(){ 
          $(this).parents(".control-group").remove();
      });

    });

</script>
@endsection