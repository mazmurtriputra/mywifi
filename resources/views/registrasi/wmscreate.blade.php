@extends('layouts.app')

@section('content')

<div class="container">
        <div class="row">
            <div class="col-12 mb-5 mt-2">
                <h3 class="p-2 text-purple-old"><i class="fa fa-user-plus"></i> Tambah Data WMS</h3>
                <hr class="my-0">
            </div>
        </div>
        @if(\Session::has('success'))
        <div class="alert alert-success">
        <p> {{ \Session::get('success') }}
        </div>
        @endif

        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
            <li class="breadcrumb-item"><a href="/registrasi">Registrasi</a></li>
            <li class="breadcrumb-item"><a href="/registrasi/wms">Daftar WMS</a></li>
            <li class="breadcrumb-item active" aria-current="page"> Formulir Pendaftaran    </li>
          </ol>
        </nav>

        {{-- Filter & Search --}}
        {{-- <div class="row">
            <div class="col-md-4 col-lg-3 mb-4">
                <a href="{{ route('dashboard.registrasi.wms') }}" class="btn bg-purple-old shadow text-light" role="button" aria-pressed="true"><i class="fa fa-user"></i> Daftar Pelanggan </a>
            </div>
          
           
        </div> --}}
        
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="card" style="border-radius: 2rem;">
            <div class="card-header text-white bg-purple-old text-center" style="
            border-radius: 2rem 2rem 0rem 0rem;">
            <h4> Formulir Pendaftaran Pelanggan WMS </h4>
            </div>
            <div class="card-body">
                <form action="{{ route('dashboard.registrasi.wmsstore')  }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div class="form-group">
                      <label>STATUS</label>
                      <select class="form-control" name="cust_status">
                        <option>LANGGANAN</option>
                      </select>
                    </div>    

                    <div class="form-group">
                      <strong>  <label>Tanggal Registrasi</label> </strong>
                        <input type="date" class="form-control" name="TGL_INPUT">
                      </div>

                    <div class="form-group">
                       <strong> <label>Nama Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="nama_plg" placeholder="Nama Pelanggan">
                          
                        </div>
                        <div class="form-group">
                         <strong> <label>Alamat Pemasangan</label></strong>
                          <input type="text" class="form-control" name="alamat" placeholder="Alamat pelanggan">
                        </div>
                        
                        <div class="form-group">
                          <strong> <label>Nomor HP Pelanggan</label> </strong>
                          <input type="text" class="form-control" name="no_hp" placeholder="Nomor HP Pelanggan">
                        </div>

                        <div class="form-group">
                           <strong> <label>Email Pelanggan</label> </strong>
                            <input type="text" class="form-control" name="email" placeholder="Email Pelanggan">
                          </div>
                          <div class="form-group">
                           <strong> <label> Layanan</label> </strong>
                            <input type="text" class="form-control" name="layanan" placeholder="Detail Layanan Paket">
                          </div>
                          <div class="form-group">
                           <strong> <label>Kecepatan</label> </strong>
                            <input type="text" class="form-control" name="NO_HP" placeholder="Kecepatan paket">
                          </div>
                          <div class="form-group">
                           <strong> <label>AP (Access Point)</label> </strong>
                            <input type="text" class="form-control" name="jml_ap" placeholder="Jumlah AP">
                          </div>
                          <div class="form-group">
                           <strong> <label>Biaya Bulanan</label></strong>
                            <input type="text" class="form-control" name="biaya_perbulan" placeholder="Biaya Bulanan">
                          </div>
                          <div class="form-group">
                           <strong> <label>PPN</label></strong>
                            <input type="text" class="form-control" name="ppn" placeholder="PPN">
                          </div>
                          <div class="form-group">
                           <strong> <label>Total Biaya</label> </strong>
                            <input type="text" class="form-control" name="total" placeholder="Total Biaya">
                          </div>
                          <div class="form-group">
                            <strong> <label>SSID</label> </strong>
                            <input type="text" class="form-control" name="ssid" placeholder="SSID">
                          </div>
                          <div class="form-group">
                           <strong> <label>Password</label> </strong>
                            <input type="text" class="form-control" name="password" placeholder="Password">
                          </div>

                          <div class="form-group">
                            <strong> <label>Kode Sales</label> </strong>
                             <input type="text" class="form-control" name="kode_sales" placeholder="Nomor Kode Sales">
                           </div>
                              <div class="form-group">
                              <strong> <label>Nama Sales</label> </strong>
                            <select name="id_sales" class="custom-select border-10">
                              @foreach($sales as $sale)
                        <option value="{{$sale->id}}" {{(isset($department->id) || old('id'))? "selected":"" }}> {{$sale->nama_sales}}</option> 
                      @endforeach    
                    </select>
                            </div>
                            
                          <div class="form-group">
                            <strong> <label>Foto Pelanggan , ODP , KTP , RUMAH</label> </strong>
                            <div class="input-group control-group increment" >
                              <input type="file" name="foto_pelanggan[]" multiple class="form-control">
                            </div>
                          </div>
                          <button type="submit" class="btn btn-lg btn-block bg-purple-old shadow text-light">Simpan</button>    
                </form>
            </div>
          </div>
           
</div>
@endsection

  
