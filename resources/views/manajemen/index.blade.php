
@extends('layouts.app')

@section('content')


{{--List User --}}

<div class="container">
    <div class="row">
        <div class="col-10 mb-5 mt-2">
            <h3 class="p-2 text-purple-old">Manajemen</h3>
            <hr class="my-0">
        </div>
    </div>

    
    <div class="row">
        <div class="col-lg-10">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/home">Beranda</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Laporan</li>
                </ol>
              </nav>
            <div class="card shadow mb-5 pb-0" style="border-radius: 1.25rem;">
                <div class="card-header bg-purple-old-fade py-2 bg-purple-old" style="
                border-radius: 1rem 1rem 0rem 0rem;">
                    <h5 class="text-white mb-0">
                        Manajemen
                    </h5>
                </div>
                <div class="dashboard card-body">

                    {{-- Pasien --}}
                    <div class="report-group mb-5">
                        <h5 class="text-purple-old">
                            Pelanggan
                        </h5>
                        <hr class="mt-1">
                        {{-- list report --}}
                        <ul class="list-group" style="border-radius: 1.25rem;">
                        <a href="{{ route('dashboard.pengaturan.sales') }}" class="list-group-item list-group-item-action">Daftar Pelanggan Deposit </a>
                            <a href="#" class="list-group-item list-group-item-action">Gangguan Pelanggan  </a>
                            <a href="#" class="list-group-item list-group-item-action">Pelanggan Cabut </a>
                          </ul>
                          
                    </div>
                    
                    {{-- Doctor --}}
        
            
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection


